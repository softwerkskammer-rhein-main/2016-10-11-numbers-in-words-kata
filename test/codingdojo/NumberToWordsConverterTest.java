package codingdojo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class NumberToWordsConverterTest {

    private NumberToWordsConverter converter;

    @Parameterized.Parameters(name = "{index}: number[{0}]= word {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, "one"},
                {2, "two"},
                {10, "ten"},
                {20, "twenty"},
                {21, "twenty one"}
        });
    }

    private int number;
    private String word;

    public NumberToWordsConverterTest(int number, String word) {
        this.number = number;
        this.word = word;
    }

    @Before
    public void setUp() {
        converter = new NumberToWordsConverter();
    }

    @Test
    public void convertNumberToWordOne() throws Exception {
        String convertedNumber = converter.convert(this.number);
        assertEquals(word, convertedNumber);
    }
}
