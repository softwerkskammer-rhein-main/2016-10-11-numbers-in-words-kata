package codingdojo;

import java.util.HashMap;
import java.util.Map;

public class NumberToWordsConverter {

    private static final Map<Integer, String> TEN_TO_TWENTY = new HashMap<Integer, String>();

    private static final Map<Integer, String> SINGLE_DIGITS = new HashMap<>();

    static {
        SINGLE_DIGITS.put(1, "one");
        SINGLE_DIGITS.put(2, "two");
        SINGLE_DIGITS.put(3, "three");
        SINGLE_DIGITS.put(4, "four");
        SINGLE_DIGITS.put(5, "five");
        SINGLE_DIGITS.put(6, "six");
        SINGLE_DIGITS.put(7, "seven");
        SINGLE_DIGITS.put(8, "eight");
        SINGLE_DIGITS.put(9, "nine");
        TEN_TO_TWENTY.put(10, "ten");
        TEN_TO_TWENTY.put(20, "twenty");
    }

    public String convert(int number) {

        if(number < 10) {
            return SINGLE_DIGITS.get(number);
        } else if (number < 21) {
            return TEN_TO_TWENTY.get(number);
        } else {
            return "twenty one";
        }

    }

}
